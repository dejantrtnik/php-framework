Manual


required if use .env file
include "Modules/database/conn.php";

samples:
$conn = new Connection('host', 'user', 'pass');
$db = $conn->connection('some_database');

sample sql query:
$sqlQuery = "SELECT * FROM some_table";
$result = mysqli_query($db, $sqlQuery);


sample json:
$data = array();
foreach ($result as $row) {
	$data[] = $row;
}
mysqli_close($conn);
echo json_encode($data);